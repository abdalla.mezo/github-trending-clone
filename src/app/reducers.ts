import dummy, { DummyState } from "./dummy";

export interface StoreState {
  dummy: DummyState;
}

const rootReducer = {
  dummy,
};

export default rootReducer;
