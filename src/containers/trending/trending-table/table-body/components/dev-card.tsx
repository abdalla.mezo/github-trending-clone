import React from "react";
import { Avatar, Box, Button, Flex, Text } from "@chakra-ui/react";
import { RiFireLine } from "react-icons/ri";
import { GoRepo } from "react-icons/go";
import { DevelopersType } from "../../../../../app/types";

const DevCard = ({ item }: { item: DevelopersType[0] }) => {
  return (
    <Flex
      p="16px"
      _notLast={{ borderBottom: "1px solid #ebedef" }}
      flex="1 1 100%"
    >
      <Box as="a" fontSize="12px" color="#586069">
        {item.rank}
      </Box>
      <Box as="a" href={item.url} w="48px" h="48px" mx="16px">
        <Avatar src={item.avatar} w="48px" h="48px" />
      </Box>
      <Flex width="calc(2/3 * 100%)">
        <Box textAlign="left" width="50%">
          <Box
            as="a"
            href={item.url}
            color="#0366d6"
            _hover={{ textDecoration: "underline" }}
            fontWeight="500"
            fontSize="20px"
            data-testid="dev-card-name"
          >
            {item.name}
          </Box>
          <Box
            as="a"
            href={item.url}
            color="#586069"
            fontSize="16px"
            fontWeight="500"
            mb="4px"
            _hover={{ color: "#0366d6" }}
            display="block"
            data-testid="dev-card-username"
          >
            {item.username}
          </Box>
        </Box>
        <Box width="50%">
          <Flex mb="4px" alignItems="center">
            <RiFireLine color="#e5534b" />
            <Text color="#586069" fontSize="12px" fontWeight="600" ml="2px">
              POPULAR REPO
            </Text>
          </Flex>
          <Flex alignItems="center">
            <GoRepo />
            <Box
              as="a"
              color="#0366d6"
              href={item.popularRepository.url}
              _hover={{ textDecoration: "underline" }}
              ml="8px"
              fontWeight="600"
              fontSize="16px"
              data-testid="popular-repo-name"
            >
              {item.popularRepository.repositoryName}
            </Box>
          </Flex>
          <Box textAlign="left" mt="4px">
            <Text fontSize="12px" color="#586069" fontWeight="500">
              {item.popularRepository.description}
            </Text>
          </Box>
        </Box>
      </Flex>
      <Flex
        width="calc(1/3 * 100%)"
        alignItems="flex-start"
        justifyContent="flex-end"
      >
        <Button
          borderRadius="6px"
          backgroundColor="#fafbfc"
          border="1px solid rgba(27, 31, 35, 0.15)"
          color="#24292e"
          p="3px 12px"
          display="flex"
          h="28px"
          textAlign="center"
        >
          Follow
        </Button>
      </Flex>
    </Flex>
  );
};

export { DevCard };
