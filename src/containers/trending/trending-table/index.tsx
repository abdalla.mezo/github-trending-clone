import { Container } from "@chakra-ui/react";
import React from "react";
import { TableBody } from "./table-body/index";
import { TableHeader } from "./table-header";

const TrendingTable = () => {
  return (
    <Container
      my="40px !important"
      maxW="container.lg"
      border="1px solid #e1e4e8"
      borderRadius="6px"
      p="0"
    >
      <TableHeader />
      <TableBody />
    </Container>
  );
};

export { TrendingTable };
