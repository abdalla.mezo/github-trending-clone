import React from "react";
import { Redirect } from "react-router";
import { routers } from "../../routes";

const RedirectToTrending = () => {
  return <Redirect to={`${routers.TRENDS}/repositories`} />;
};

export default RedirectToTrending;
