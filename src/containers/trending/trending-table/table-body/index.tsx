import { Box } from "@chakra-ui/layout";
import React from "react";
import data from "../../../../app/mock-repos.json";
import dataDevs from "../../../../app/mock-devs.json";
import { DevCard } from "./components/dev-card";
import { RepoCard } from "./components/repo-card";
import { useRouteMatch } from "react-router-dom";
import { DevelopersType, ReposType } from "../../../../app/types";

interface TableItemProps {
  item: ReposType[0] | DevelopersType[0];
  isRepos: boolean;
}

const TableItem = (props: TableItemProps) => {
  const { item, isRepos } = props;

  if (isRepos) {
    return <RepoCard item={item as ReposType[0]} />;
  }

  return <DevCard item={item as DevelopersType[0]} />;
};

const TableBody = () => {
  const { params } = useRouteMatch();

  const isRepos =
    (params as unknown as { trendingEntity: string })?.trendingEntity ===
    "repositories";

  let dataSource = isRepos ? data : dataDevs;

  return (
    <Box>
      {dataSource.map((item, index) => (
        <TableItem item={item} key={index} isRepos={isRepos} />
      ))}
    </Box>
  );
};

export { TableBody };
