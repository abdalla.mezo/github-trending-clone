import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface DummyState {
  isLoading: boolean;
}

const initialState: DummyState = {
  isLoading: false,
};

const setIsLoadingAction = (
  state: DummyState,
  action: PayloadAction<boolean>
) => {
  state.isLoading = action.payload;
};

const metaReducer = createSlice({
  name: "dummy",
  initialState,
  reducers: {
    setIsLoadingAction,
  },
});

export const { setIsLoadingAction: setIsCollectionsLoading } =
  metaReducer.actions;

export default metaReducer.reducer;
