import * as React from "react";
import { Box, VStack, Grid } from "@chakra-ui/react";
import { TrendingHero } from "./trending-hero";
import { TrendingTable } from "./trending-table";
import { Redirect, Route, Switch, useRouteMatch } from "react-router-dom";

const Trending = () => {
  const { path } = useRouteMatch();

  return (
    <Box textAlign="center" fontSize="xl">
      <Grid minH="100vh">
        <VStack>
          <TrendingHero />
          <Switch>
            <Route path={`${path}/:trendingEntity`}>
              <TrendingTable />
            </Route>
            <Route path={path} exact>
              <Redirect to={`${path}/repositories`} />
            </Route>
          </Switch>
        </VStack>
      </Grid>
    </Box>
  );
};

export default Trending;
