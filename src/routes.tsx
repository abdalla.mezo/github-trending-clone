import React from "react";
import { lazy } from "react";
import RedirectToTrending from "./containers/redirect";

export const routers = {
  REDIRECT_PATH: "/",
  TRENDS: "/trending",
};

export const routes = [
  {
    path: routers.REDIRECT_PATH,
    component: () => <RedirectToTrending />,
    exact: true,
  },
  {
    path: routers.TRENDS,
    component: lazy(() => import("./containers/trending")),
  },
];
