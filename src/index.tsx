import * as React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { QueryClientProvider, QueryClient } from "react-query";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ChakraProvider, theme } from "@chakra-ui/react";
import reportWebVitals from "./reportWebVitals";
import * as serviceWorker from "./serviceWorker";
import store from "./app/store";
import { routes } from "./routes";
import { Header } from "./components/header";

const queryClient = new QueryClient();

const WrappedApp = () => (
  <Provider store={store}>
    <QueryClientProvider client={queryClient}>
      <ChakraProvider theme={theme}>
        <React.Suspense fallback={<div>Loading ..</div>}>
          <Router>
            <Header />
            <Switch>
              {routes.map((route) => (
                <Route {...route} key={route.path} />
              ))}
            </Switch>
          </Router>
        </React.Suspense>
      </ChakraProvider>
    </QueryClientProvider>
  </Provider>
);

ReactDOM.render(
  <React.StrictMode>
    <WrappedApp />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorker.unregister();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
