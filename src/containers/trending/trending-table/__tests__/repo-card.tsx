import React from "react";
import { render, RenderResult } from "@testing-library/react";
import repoJson from "../../../../app/mock-repos.json";
import { RepoCard } from "../table-body/components/repo-card";

describe("<RepoCard />", () => {
  let component: RenderResult;

  const repoExample = repoJson[0];

  beforeEach(() => {
    component = render(<RepoCard item={repoExample} />);
  });

  it("Should render developer information correctly", () => {
    const { getByTestId } = component;
    const repoDescription = getByTestId("repo-description");

    expect(repoDescription.innerHTML).toEqual(repoExample.description);
  });

  it("Should render correct stars", () => {
    const { getByTestId } = component;
    const repoStars = getByTestId("repo-stars");
    const repoForks = getByTestId("repo-forks");

    expect(repoStars.innerHTML).toEqual(`&nbsp;${repoExample.totalStars}`);
    expect(repoForks.innerHTML).toEqual(`&nbsp;${repoExample.forks}`);
  });
});
