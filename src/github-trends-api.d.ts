declare module "github-trends-api" {
  interface Options {
    section: "developers" | "repositories";
  }

  export default (options: Options) => new Promise();
}
