import { Box, Text } from "@chakra-ui/layout";
import React from "react";

const TrendingHero = () => {
  return (
    <Box
      textAlign="center"
      py="40px"
      mx="auto"
      background="#fafbfc"
      borderBottom="1px solid #e1e4e8"
      w="100%"
    >
      <Text variant="h1" fontSize="32px" fontWeight="600">
        Trending
      </Text>
      <Text variant="p" fontSize="16px">
        See what the GitHub community is most excited about today.
      </Text>
    </Box>
  );
};

export { TrendingHero };
