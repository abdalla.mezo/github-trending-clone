import devs from "./mock-devs.json";
import repos from "./mock-repos.json";

export type DevelopersType = typeof devs;
export type ReposType = typeof repos;
