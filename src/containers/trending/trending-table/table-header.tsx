import { Box, BoxProps, Flex } from "@chakra-ui/react";
import React from "react";
import { Link, useRouteMatch } from "react-router-dom";

const LinkButton = (props: BoxProps) => (
  <Box
    as="a"
    border="1px solid"
    fontSize="14px"
    padding="5px 16px"
    fontWeight="600"
    cursor="pointer"
    backgroundColor="#e1e4e8"
    borderColor="#e1e4e8"
    {...props}
  />
);

const getUrl = (url: string, entity: string) => {
  const urlChunks = url.split("/");
  urlChunks.pop();
  urlChunks.push(entity);
  return urlChunks.join("/");
};

const DevRepoSwitch = () => {
  const { url, params } = useRouteMatch();

  const isRepos =
    (params as unknown as { trendingEntity: string })?.trendingEntity ===
    "repositories";

  return (
    <Flex>
      <Link to={getUrl(url, "repositories")}>
        <LinkButton
          backgroundColor={isRepos ? "#0366d6" : "#f6f8fa"}
          borderColor={isRepos ? "#0366d6" : "#e1e4e8"}
          borderBottomLeftRadius="6px"
          borderTopLeftRadius="6px"
          color={isRepos ? "#fff" : "#000"}
        >
          Repositories
        </LinkButton>
      </Link>
      <Link to={getUrl(url, "developers")}>
        <LinkButton
          backgroundColor={!isRepos ? "#0366d6" : "#f6f8fa"}
          borderColor={!isRepos ? "#0366d6" : "#e1e4e8"}
          borderBottomRightRadius="6px"
          borderTopRightRadius="6px"
          color={!isRepos ? "#fff" : "#000"}
        >
          Developers
        </LinkButton>
      </Link>
    </Flex>
  );
};

const TableHeader = () => {
  return (
    <Flex backgroundColor="#f6f8fa" borderBottom="1px solid #e1e4e8" p="16px">
      <Box>
        <DevRepoSwitch />
      </Box>
      <Box></Box>
    </Flex>
  );
};

export { TableHeader };
