import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { DevCard } from "../table-body/components/dev-card";
import devJson from "../../../../app/mock-devs.json";

describe("<DevCard />", () => {
  let component: RenderResult;

  const devExample = devJson[0];

  beforeEach(() => {
    component = render(<DevCard item={devExample} />);
  });

  it("Should render developer information correctly", () => {
    const { getByTestId } = component;
    const devName = getByTestId("dev-card-name");
    const devUsername = getByTestId("dev-card-username");

    expect(devName.innerHTML).toEqual(devExample.name);
    expect(devUsername.innerHTML).toEqual(devExample.username);
  });

  it("Should render correct repo information", () => {
    const { getByTestId, debug } = component;
    const repoName = getByTestId("popular-repo-name");

    expect(repoName.innerHTML).toEqual(
      devExample.popularRepository.repositoryName
    );
    expect(repoName).toHaveAttribute("href", devExample.popularRepository.url);
  });
});
