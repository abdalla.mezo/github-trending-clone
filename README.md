# COINMENA ASSESSMENT

Simple github trending page clone.

---

## Technologies

- react
- react-router
- react-query
- chakra-ui
- typescript
- jest
- testing-library

## Notes

- APIs are failing, tries more than an approach, due to time I had mocked the api calls to achieve the required UI for now.
- Skipped trending filters as it will be only ui, as I was not able to connect to the actual api.
- used react-router to create a nested routes in the trending route, while the rest of the routes will redirect to it.
- implemented only the urls href as those only the data I have, i was not able to implement `follow`, `star` actions as it will require more time (authenticating with github api to get user information)
