import { useQuery } from "react-query";
import trendingApi from "github-trends-api";

const useGetTrendingReposQuery = () =>
  useQuery("trendingRepos", () =>
    fetch("https://gh-trending-api.herokuapp.com/repositories").then(
      (res) => res
    )
  );

const useGetTrendingUsersQuery = () =>
  useQuery(
    "trendingUsers",
    async () => await trendingApi({ section: "developers" })
  );

export { useGetTrendingReposQuery, useGetTrendingUsersQuery };
