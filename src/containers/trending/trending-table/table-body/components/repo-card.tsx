import React from "react";
import { GoRepo, GoRepoForked } from "react-icons/go";
import { AiOutlineStar } from "react-icons/ai";
import { Avatar, Box, Button, Flex, Text } from "@chakra-ui/react";
import { ReposType } from "../../../../../app/types";

const RepoCard = ({ item }: { item: ReposType[0] }) => {
  return (
    <Flex
      justifyContent="space-between"
      p="16px"
      _notLast={{ borderBottom: "1px solid #ebedef" }}
      flex="1 1 100%"
    >
      <Box width="100%">
        <Flex alignItems="center" justifyContent="space-between">
          <Flex alignItems="center">
            <GoRepo />
            <Box
              as="a"
              color="#0366d6"
              href={item.url}
              _hover={{ textDecoration: "underline" }}
              ml="16px"
              fontWeight="500"
              fontSize="20px"
            >
              {item.username} / {item.repositoryName}
            </Box>
          </Flex>
          <Box>
            <Button
              borderRadius="6px"
              backgroundColor="#fafbfc"
              border="1px solid rgba(27, 31, 35, 0.15)"
              color="#24292e"
              p="3px 12px"
              display="flex"
              h="28px"
              textAlign="center"
            >
              <AiOutlineStar />
              <Text fontSize="14px" fontWeight="600" ml="6px">
                Star
              </Text>
            </Button>
          </Box>
        </Flex>
        <Text
          my="4px"
          fontSize="14px"
          color="#586069"
          w="75%"
          textAlign="left"
          data-testid="repo-description"
        >
          {item.description}
        </Text>
        <Flex
          mt="8px"
          justifyContent="space-between"
          alignItems="center"
          w="100%"
        >
          <Flex alignItems="center">
            {item.language && (
              <Text fontSize="12px" color="#586069" mr="16px">
                {item.language}
              </Text>
            )}
            <Flex alignItems="center" mr="16px">
              <AiOutlineStar />
              <Text
                as="span"
                fontSize="12px"
                color="#586069"
                data-testid="repo-stars"
              >
                &nbsp;{item.totalStars}
              </Text>
            </Flex>
            <Flex alignItems="center" mr="16px">
              <GoRepoForked />
              <Text
                as="span"
                fontSize="12px"
                color="#586069"
                data-testid="repo-forks"
              >
                &nbsp;{item.forks}
              </Text>
            </Flex>
            <Flex alignItems="center">
              <Text as="span" fontSize="12px" color="#586069" mr="8px">
                Built by
              </Text>
              <Flex>
                {item.builtBy.map((user: any) => (
                  <Box
                    as="a"
                    href={user.url}
                    pb="0"
                    textDecoration="none"
                    w="20px"
                    h="20px"
                    mr="2px"
                    key={user.url}
                  >
                    <Avatar
                      src={user.avatar}
                      key={user.url}
                      w="20px"
                      h="20px"
                    />
                  </Box>
                ))}
              </Flex>
            </Flex>
          </Flex>
          <Flex>
            <AiOutlineStar />
            <Text as="span" fontSize="12px" color="#586069" ml="8px">
              {item.starsSince} Stars since today
            </Text>
          </Flex>
        </Flex>
      </Box>
    </Flex>
  );
};

export { RepoCard };
